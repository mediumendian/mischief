package list.simple_list;

/**
 * Created by ra on 02.04.15. Simple generic list tester.
 */
public class SimpleListTest {
    public static void main(String args[]) {
        Node<Integer> n1 = new Node<Integer>(1);
        Node<Integer> n2 = new Node<Integer>(2);
        Node<Integer> n3 = new Node<Integer>(3);
        Node<Integer> n4 = new Node<Integer>(4);

        Node<String> s1 = new Node<String>("This");
        Node<String> s2 = new Node<String>("might");
        Node<String> s3 = new Node<String>("even");
        Node<String> s4 = new Node<String>("work.");

        Node<Boolean> b1 = new Node<Boolean>(true);
        Node<Boolean> b2 = new Node<Boolean>(false);
        Node<Boolean> b3 = new Node<Boolean>(false);
        Node<Boolean> b4 = new Node<Boolean>(true);

        List<Integer> l1 = new List<Integer>(n1, n2, n3, n4);

        List<String> l2 = new List<String>(s1, s2, s3, s4);

        List<Boolean> l3 = new List<Boolean>();
        l3.append(b1);
        l3.append(b3);
        l3.append(b4);
        l3.append(b2);
        l3.delete_byVal(true);
        l2.delete_byVal("work.");

        System.out.printf("The list l1 contains %d elements,\nthe list l3 contains %d elements.\n", l1.numberOfElems, l3.numberOfElems);
        l2.display();
        l3.display();

    }
}
