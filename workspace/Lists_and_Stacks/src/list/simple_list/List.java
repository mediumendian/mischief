package list.simple_list;

/**
 * Created by ra on 01.04.15. Simple generic linked list.
 */

public class List<T> {
    public int numberOfElems;
    private Node<T> front;
    private Node<T> rear;

    public List(Node<T>... ns) {
        for (Node<T> node : ns) {
            append(node);
        }
    }

    public boolean isEmpty() {
        return front == null;
    }

    public void append(Node<T> n) {
        if (isEmpty()) {
            front = n;
            rear = n;
        } else {
            rear.setNext(n);
            rear = n;
        }
        numberOfElems++;
    }

    public void display() {
        for (Node<T> it = front; it.next != null; it = it.next) {
            System.out.print(it + ", ");
        }
        System.out.println(rear.getData().toString());
    }

    public void insert(Node<T> it, T dataVal) {
        assert it != null : "cannot insert after null!";
        Node<T> n = new Node<T>(dataVal);

        if (it.next == null) {
            rear = n;
        } else {
            n.next = it.next;
        }
        it.next = n;
        numberOfElems++;
    }

    public void addAt(int index, T dataVal) {
        Node node = front;

        if (index == 0) {
            prepend(dataVal);
        } else {
            for (int i = 1; node != null && i < index; ++i)
                node = node.next;

            if (node != null) insert(node, dataVal);
        }

    }

    public void prepend(T n) {
        Node<T> node = new Node<T>(n);
        node.next = front;
        if (node.next == null) {
            rear = node;
        }
        front = node;
        numberOfElems++;
    }

    public void delete_byVal(T dataVal) {
        if (front != null && front.getData().equals(dataVal)) {
            front = front.next;
            if (front == null) {
                rear = null;
            }
            numberOfElems--;
        } else if (front != null) {
            Node<T> it = front;
            while (it.next != null) {
                Node<T> n = it.next;
                if (n.getData().equals(dataVal)) {
                    if (n.next == null) {
                        rear = it;
                    }
                    it.next = n.next;
                    numberOfElems--;
                    break;
                }
            }
        }
    }


}
